# OpenIPMC-HW DIMM Manufacturer Tester Board (MTB)

PCB intended to program and test OpenIPMC-HW at the assembly house.

## License

OpenIPMC-HW MTB is Copyright 2020-2021 of Luis Ardila-Perez. Luigi Calligaris, and Andre Cascadan. OpenIPMC-HW MBT is released under "CERN Open Hardware Licence Version 2 - Permissive" license. Please refer to the LICENSE document included in this repository.

